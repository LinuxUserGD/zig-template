const std = @import("std");
const print_fmt = std.debug.print;

pub fn main() !void {
    const init = @import("init.zig");
    const args = try getArgs();
    for (args) |arg| {
        if (std.mem.eql(u8, "version", arg)) {
            version_info();
            return;
        }
        if (std.mem.eql(u8, "help", arg)) {
            help();
            return;
        }
        if (std.mem.eql(u8, "test=vector2", arg)) {
            run_vector2();
            return;
        }
        if (std.mem.eql(u8, "benchmark", arg)) {
            run_benchmark();
            return;
        }
        if (std.mem.eql(u8, "test=parser", arg)) {
            run_parser();
            return;
        }
        const path_format_arg = "format=";
        if (begins_with(arg, path_format_arg) and ends_with(arg, ".gd")) {
            const format = true;
            const comp = false;
            start(format, comp);
            return;
        }
        const path_exp_arg = "exp=";
        if (begins_with(arg, path_exp_arg)) {
            start_exp();
            return;
        }
        const compile_arg = "compile=";
        if (begins_with(arg, compile_arg) and ends_with(arg, ".gd")) {
            const format = true;
            const comp = true;
            start(format, comp);
            return;
        }
        const setup_arg = "setup=";
        if (begins_with(arg, setup_arg)) {
            try setup(init.setuptools_min_ver, init.package_name, init.author, init.author_email, init.project_url, init.download_url, init.documentation_url, init.source_url, init.tracker_url, init.description, init.proj_license);
            return;
        }
    }
    help();
    return;
}

fn help() void {
    const VER_DESC = "show program's version number and exit";
    const HELP_DESC = "show this help message and exit";
    const FMT_DESC = "transpile and format GDScript files recursively";
    const COMP_DESC = "compile GDScript file to binary using Clang/Nuitka";
    const EXP_DESC = "experimental option to tokenize GDScript file";
    const SETUP_DESC = "generate a setup.py and pyproject.toml file to install Python project";
    const VEC2_DESC = "testing Vector2 implementation";
    const PARSER_DESC = "running GDScript tests (not working yet)";
    const BENCH_DESC = "running benchmark to compare performance";
	print("Usage: gds [options]");
	print("");
	print("Options:");
	print("  " ++ "version                           " ++ VER_DESC);
	print("  " ++ "help                              " ++ HELP_DESC);
	print("  " ++ "format=../path/to/file.gd         " ++ FMT_DESC);
	print("  " ++ "compile=../path/to/file.gd        " ++ COMP_DESC);
	print("  " ++ "exp=../path/to/file.gd            " ++ EXP_DESC);
	print("  " ++ "setup=../path/setup.py            " ++ SETUP_DESC);
	print("  " ++ "test=vector2                      " ++ VEC2_DESC);
	print("  " ++ "test=parser                       " ++ PARSER_DESC);
	print("  " ++ "benchmark                         " ++ BENCH_DESC);
}

fn version_info() void {
    const major = 4;
    const minor = 3;
    const patch = 0;
    const status = "rc";
    const build = "gentoo";
    const id = "e343dbb";
    print("GDScript Transpiler " ++ "0.1.6" ++ "\n");
    const GDV = std.fmt.comptimePrint("{}.{}.{}", .{major, minor, patch});
	print("Compatible with Godot" ++ "\n" ++ GDV ++ "." ++ status ++ "." ++ build ++ "." ++ id);
	print("Python");
	print("3.12.4 (main, Aug  2 2024, 23:30:48) [Clang 19.1.0+libcxx ]");
	print("Nuitka");
	print("2.4.5");
	print("Ruff");
	print("0.5.5");
	print("Zig");
	print("0.14.0-dev.839+a931bfada");
}

fn run_vector2() void {
    
}

fn run_benchmark() void {
    
}

fn run_parser() void {
    
}

fn start(format: bool, compile: bool) void {
    if (format) {
        print("format");
    }
    if (compile) {
        print("compile");
    }
    
}

fn start_exp() void {
    
}

fn setup(comptime setuptools_min_ver: comptime_int, comptime package_name: []const u8, comptime author: []const u8, comptime author_email: []const u8, comptime project_url: []const u8, comptime download_url: []const u8, comptime documentation_url: []const u8, comptime source_url: []const u8, comptime tracker_url: []const u8, comptime description: []const u8, comptime proj_license: []const u8) !void {
    const transpiler = @import("transpiler.zig");
    const pyproject_path = "pyproject" ++ "." ++ "toml";
	const pycontent = comptime transpiler.generate_pyproject(setuptools_min_ver);
    try transpiler.save(pyproject_path, pycontent);
    const setup_path = "setup" ++ "." ++ "py";
	const setupcontent = comptime transpiler.generate_setup(package_name, author, author_email, project_url, download_url, documentation_url, source_url, tracker_url, description, proj_license);
	try transpiler.save(setup_path, setupcontent);
}

inline fn getArgs() ![][:0]u8 {
    return std.process.argsAlloc(std.heap.page_allocator);
}

inline fn print(str: []const u8) void {
    print_fmt("{s}\n", .{str});
}

inline fn begins_with(str: []const u8, search: []const u8) bool {
    return std.mem.eql(u8, str[0 .. search.len], search);
}

inline fn ends_with(str: []const u8, search: []const u8) bool {
    return std.mem.eql(u8, str[str.len-search.len .. str.len], search);
}