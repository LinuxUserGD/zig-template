const std = @import("std");

const props = @import("props.zig");

pub fn generate_setup(comptime package_name: []const u8, comptime author: []const u8, comptime author_email: []const u8, comptime project_url: []const u8, comptime download_url: []const u8, comptime documentation_url: []const u8, comptime source_url: []const u8, comptime tracker_url: []const u8, comptime description: []const u8, comptime proj_license: []const u8) []const u8 {
    comptime {
    const file = props.get_setup(package_name, author, author_email, project_url, download_url, documentation_url, source_url, tracker_url, description, proj_license);
    const content = appendArray(file);
    const res = std.fmt.comptimePrint("{s}", .{content});
    return res;
    }
}

pub fn generate_pyproject(comptime setuptools_min_ver: comptime_int) []const u8 {
    comptime {
    const file = props.get_pyproject_toml(setuptools_min_ver);
    const content = appendArray(file);
    const res = std.fmt.comptimePrint("{s}", .{content});
    return res;
    }
}

fn appendArray(comptime array: []const []const u8) []const u8 {
    comptime {
        var xlen = 0;
        const linebreak = "\n";
        for (array) |elem| {
            xlen += elem.len;
            xlen += linebreak.len;
        }
        var output = [_]u8{undefined} ** xlen;
        xlen = 0;
        for (array) |elem| {
            @memcpy(output[xlen..xlen+elem.len+linebreak.len], elem ++ linebreak);
            xlen += elem.len;
            xlen += linebreak.len;
        }
        const res = std.fmt.comptimePrint("{s}", .{output});
        return res;
    }
}

pub fn save(path: []const u8, content: []const u8) !void {
    var file = try std.fs.cwd().createFile(path, .{});
    defer file.close();
    try file.writeAll(content);
}