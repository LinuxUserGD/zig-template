pub const setuptools_min_ver = 42;
pub const package_name = "gdsbin";
pub const author = "LinuxUserGD";
pub const author_email = "hugegameartgd@gmail.com";
pub const project_url = "https://codeberg.org/LinuxUserGD/gdscript-transpiler-bin";
pub const download_url = "https://linuxusergd.itch.io/gdscript-transpiler-bin";
pub const documentation_url = "https://codeberg.org/LinuxUserGD/gdscript-transpiler-bin/wiki";
pub const source_url = "https://codeberg.org/LinuxUserGD/gdscript-transpiler-bin";
pub const tracker_url = "https://codeberg.org/LinuxUserGD/gdscript-transpiler-bin/issues";
pub const description = "GDScript and Python runtime environment";
pub const proj_license = "MIT";